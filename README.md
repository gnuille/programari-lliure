# Programari lliure i implicacions polítiques.

Text en etapes molt primerenques desenvolupament.

## Proposta de estructura

- Què és el programari lliure?

- Implicacions i reflexions sobre el programari lliure
    - Sobre la llibertat 0
    - Sobre la llibertat 1
    - Sobre la llibertat 2
    - Sobre la llibertat 3

- Sobre la propietat intel·lectual
- Conclusions

## Contribucions

Tota ajuda es benvinguda! Si teniu comentaris o correccions podeu fer-les arribar de diverses maneres:
- Fent servir les "Issues" del repositori.
- Enviant un correu a teleportex AT disroot.org
- Per twitter a @gnuille

Moltes gràacies!!!

